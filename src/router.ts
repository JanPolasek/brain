﻿const router = [
    {
        handler: 'Basic.ping',
        selector: ''
    },
    {
        handler: 'Roll.rollt',
        selector: 'rollt'
    },
    {
        handler: 'CustomCommands.list',
        selector: 'list'
    },
    {
        handler: 'CustomCommands.make',
        selector: 'make'
    },
    {
        handler: 'CustomCommands.remove',
        selector: 'remove'
    },
    {
        handler: 'CurrencyConverter.handler',
        selector: 'cash'
    },
    {
        handler: 'Temperature.handler',
        selector: 'temp'
    },
    {
        handler: 'Converter.handler',
        selector: 'convert'
    },
    {
        handler: 'Basic.quote',
        selector: 'quote'
    },
    {
        handler: 'Basic.osr',
        selector: 'osr'
    },
    {
        handler: 'Twitch.when',
        selector: 'when'
    },
    {
        handler: [
            {
                handler: 'create',
                selector: '*'
            },
            {
                handler: 'scores',
                selector: 'scores'
            }
        ],
        selector: 'trivia',
        router: 'Trivia'
    },
    {
        handler: [
            {
                handler: 'set',
                selector: 'set'
            }, {
                handler: 'get',
                selector: '*'
            }, {
                handler: 'list',
                selector: 'list'
            }, {
                handler: 'remove',
                selector: 'remove'
            }
        ],
        selector: 'time',
        router: 'Timezones'
    },
    {
        handler: [
            {
                handler: 'set',
                selector: 'set'
            }, {
                handler: 'get',
                selector: '*'
            }, {
                handler: 'list',
                selector: ''
            }, {
                handler: 'make',
                selector: 'make'
            },
            {
                handler: 'find',
                selector: 'find'
            },
            {
                handler: 'remove',
                selector: 'remove'
            }/*,
            {
                handler: 'help',
                selector: 'help'
            }*/
        ],
        selector: 'games',
        router: 'Games'
    },
    {
        handler: [
            {
                handler: 'mathdocs',
                selector: 'docs'
            }, {
                handler: 'math',
                selector: '*'
            }
        ],
        selector: 'imalazyfuck',
        router: 'Basic'
    },
    {
        handler: 'Pin.pin',
        selector: 'pin'
    },
    {
        handler: [
            {
                handler: 'help',
                selector: '*'
            },
            {
                handler: 'help',
                selector: ''
            }, {
                handler: 'games',
                selector: 'games'
            }, {
                handler: 'time',
                selector: 'time'
            }, {
                handler: 'custom',
                selector: 'custom'
            },
            {
                handler: 'math',
                selector: 'imalazyfuck'
            },
            {
                handler: 'convert',
                selector: 'convert'
            },
            {
                handler: 'temp',
                selector: 'temp'
            },
            {
                handler: 'cash',
                selector: 'cash'
            },
            {
                handler: 'trivia',
                selector: 'trivia'
            }
        ],
        selector: 'help',
        router: 'Help'
    }
];

export default router;

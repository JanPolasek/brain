﻿import * as Discord from 'discord.js';
import * as levenshtein from 'fast-levenshtein';
import * as lowdb from 'lowdb';
import * as FileAsync from 'lowdb/adapters/FileAsync';
import { Services, EmoteService, IAdditionalData, IMap } from './discordbot';
import { Resolver } from './resolver';

export class Bot {
    private commands: any = {};
    private bot: Discord.Client;
    private strict: boolean;
    private custom: boolean;
    private commandPrefix: string;
    private commandPrefixType: string;
    private commandPrefixTypos: string[];
    private lurkHandler;
    private additionalData: IAdditionalData = {};
    private customHandler;
    private db;
    private handlers;
    private strictness: string;
    private levenshteinDistance: number;
    private locks: IMap<Resolver> = {};
    private services: Services;

    private ping: any = {
        handler: null,
        action: null
    };

    constructor(config: any) {
        const self = this;
        self.bot = new Discord.Client();

        const adapter = new FileAsync(config.jsonStorePath);
        lowdb(adapter).then((db) => {
            self.db = db;
            self.db.defaults({
                timezones: {},
                games: [],
                commandList: [],
                customCommands: {},
                quotes: [],
                scores: []
            }).write().then(() => {
                self.onError();
                self.onMessage();
                self.bot.login(config.token);
                self.onReady();
            });
        });
    }

    public router(config: any): void {
        if(config.handlers) {
            this.handlers = {};
            for(const c in config.handlers) {
                if(config.handlers.hasOwnProperty(c)) {
                    this.handlers[config.handlers[c].name] = new config.handlers[c]();
                }
            }
        } else {
            throw new Error('List of handler modules is required');
        }
        if(config.customCommandHandler) {
            this.customCommands(config.customCommandHandler);
        }
        if(config.commandPrefix) {
            this.commandPrefix = config.commandPrefix;
            this.commandPrefixType = config.commandPrefix.length > 1 ? 'string' : 'char';
        } else {
            throw new Error('Undefined command prefix.');
        }
        if(config.commandPrefixTypos) {
            this.commandPrefixTypos = config.commandPrefixTypos;
        }

        this.strict = config.strict === undefined ? false : config.strict;

        if(config.strictMode) {
            if(config.strictMode.enabled) {
                this.strictness = 'strict';
            } else if(config.strictMode.sensitivityType === 'basic') {
                this.strictness = 'basic';
            } else if(config.strictMode.sensitivityType === 'levenshtein'
                && typeof config.strictMode.distance === 'number') {
                this.strictness = 'levenshtein';
                this.levenshteinDistance = config.strictMode.distance;
            } else {
                throw new Error('Invalid strictness config');
            }
        } else {
            this.strictness = 'strict';
        }

        for(const command of config.routes) {
            if(typeof command.handler as any !== 'object') {
                const handlerSplit = command.handler.split('.');
                const handlerClass = handlerSplit[0];
                const handlerMethod = handlerSplit[1];
                if(command.selector != null && command.selector !== '') {
                    this.commands[command.selector] = {};
                    this.commands[command.selector].handler = handlerMethod;
                    this.commands[command.selector].handlerClass = this.handlers[handlerClass];
                } else {
                    this.ping.handler = handlerMethod;
                    this.ping.handlerClass = this.handlers[handlerClass];
                }
            } else {
                this.commands[command.selector] = {};
                this.commands[command.selector].handler = this.handlers[command.router].route.name;
                this.commands[command.selector].handlerClass = this.handlers[command.router];
                this.handlers[command.router].setRouter(command.handler, this.commandPrefixType);
            }
        }

        this.additionalData.commandList = Object.keys(this.commands);
    }

    public lurker(handler) {
        const split = handler.split('.');
        this.lurkHandler = {};
        this.lurkHandler.handlerClass = this.handlers[split[0]];
        this.lurkHandler.handler = split[1];
    }

    private customCommands(handlerName: string): void {
        this.custom = true;

        const customCommandClass = handlerName.split('.')[0];
        const customCommandHandler = handlerName.split('.')[1];
        const customHandler = this.handlers[customCommandClass];

        if(customHandler[customCommandHandler]) {
            this.customHandler = {};
            this.customHandler.handler = customCommandHandler;
            this.customHandler.handlerClass = customHandler;
        } else {
            throw new Error('Custom command class handler is undefined');
        }
    }

    private onReady(): void {
        const self = this;
        self.bot.on('ready', () => {
            self.services = new Services(self.db, self.additionalData, new EmoteService(self.bot));
            console.log('Discord bot ready');
        });
    }

    private getCommandSelector(str, commandPrefix) {
        let commandMatch = false;
        let commandSelector = '';
        let commandSelectorMulti = '';
        const split = str.split(' ');

        if(this.commandPrefixType === 'string') {
            commandSelector = split.length > 1 ? split[1] : null;
            commandSelectorMulti = split.slice(1).join(' ');
            if(this.strictness === 'strict') {
                commandMatch = split[0] === commandPrefix ? true : false;
            } else {
                commandMatch = split[0].toLowerCase() === commandPrefix.toLowerCase() ? true : false;
            }

        } else {
            commandMatch = str.charAt(0) === commandPrefix ? true : false;
            commandSelector = split[0].substring(1);
            split[0] = split[0].substring(1);
            commandSelectorMulti = split.join(' ');
        }

        return {
            isCommand: commandMatch,
            single: commandSelector,
            multi: commandSelectorMulti
        };
    }

    private getStaticHandler(cmd) {
        if(this.strictness === 'strict' &&
            (this.commands[cmd.single] || this.commands[cmd.multi])) {
            let selector;
            if(this.commands[cmd.single]) {
                selector = cmd.single;
            } else if(this.commands[cmd.multi]) {
                selector = cmd.multi;
            }
            return this.commands[selector];
        } else if(this.strictness !== 'strict') {
            let command;
            for(const key in this.commands) {
                if(this.commands.hasOwnProperty(key)) {
                    const convertedKey = this.convertToBasic(key);
                    const convertedCommandSelector = this.convertToBasic(cmd.single);
                    const convertedCommandSelectorMulti = this.convertToBasic(cmd.multi);

                    if(convertedKey === convertedCommandSelector) {
                        command = this.commands[key];
                        break;
                    }
                    if(!command && convertedKey === convertedCommandSelectorMulti) {
                        command = this.commands[key];
                        break;
                    }
                }
            }
            return command;
        }
    }

    private getLevenshteinHandler(cmd) {
        let command;
        let minDistance = 100000;
        let levenshteinSingle;
        let levenshteinMulti;
        for(const key in this.commands) {
            if(this.commands.hasOwnProperty(key)) {
                const convertedKey = this.convertToBasic(key);
                const convertedCommandSelector = this.convertToBasic(cmd.single);
                const convertedCommandSelectorMulti = this.convertToBasic(cmd.multi);

                if(this.strictness === 'levenshtein') {
                    levenshteinSingle = levenshtein.get(convertedKey, convertedCommandSelector);
                    levenshteinMulti = levenshtein.get(convertedKey, convertedCommandSelectorMulti);
                    if(!command && levenshteinSingle <= this.levenshteinDistance && levenshteinSingle < minDistance) {
                        command = this.commands[key];
                        minDistance = levenshteinSingle;
                    }
                    if(!command && levenshteinSingle <= this.levenshteinDistance && levenshteinMulti < minDistance) {
                        command = this.commands[key];
                        minDistance = levenshteinMulti;
                    }
                }
            }
        }

        return command;
    }

    private onMessage() {
        this.bot.on('message', async (msg: Discord.Message) => {
            let resolver = new Resolver();
            const commandHandlers = {
                static: undefined,
                custom: this.customHandler,
                ping: this.ping,
                lurker: this.lurkHandler,
                levenshtein: undefined
            };

            if(msg.author.bot) {
                return;
            }

            const message = msg.content;
            const commandSelector = this.getCommandSelector(message, this.commandPrefix);

            msg.content = msg.content.replace(/\s\s+/g, ' ');

            if(commandSelector.isCommand === false) {
                for(const typo of this.commandPrefixTypos) {
                    if(this.getCommandSelector(message, typo).isCommand) {
                        msg.channel.send('I\'m not ' + typo + ', ya goober');
                    }
                }

                if(this.locks[msg.channel.id]) {
                    const command = this.locks[msg.channel.id].lock.split('.');
                    const [commandClass, commandMethod] = command;
                    resolver = await this.handlers[commandClass][commandMethod](
                        msg, this.services
                    );
                    if(resolver.lock === null) {
                        this.unregisterLock(msg.channel.id);
                    }
                }

                resolver = await commandHandlers.lurker.handlerClass[commandHandlers.lurker.handler](
                    msg, this.services
                );
                return;
            }

            if(commandSelector.single === null) {
                resolver = await commandHandlers.ping.handlerClass[commandHandlers.ping.handler](
                    msg, this.services
                );
                this.registerLock(resolver);
                return;
            }

            commandHandlers.static = this.getStaticHandler(commandSelector);
            if(this.strictness === 'levenshtein') {
                commandHandlers.levenshtein = this.getLevenshteinHandler(commandSelector);
            }

            if(commandHandlers.static) {
                resolver = await commandHandlers.static.handlerClass[commandHandlers.static.handler](
                    msg, this.services
                );
                this.registerLock(resolver);
                return;
            }

            if(commandHandlers.custom) {
                resolver = await commandHandlers.custom.handlerClass[commandHandlers.custom.handler](
                    msg, this.services
                );
                if(resolver.resolved === true) {
                    this.registerLock(resolver);
                    return;
                }
            }
            if(commandHandlers.levenshtein && resolver.resolved === false) {
                resolver = await commandHandlers.levenshtein.handlerClass[commandHandlers.levenshtein.handler](
                    msg, this.services
                );
                setTimeout(() => {
                    msg.reply('the command was misspelled, you dingus');
                }, 20);
                this.registerLock(resolver);
            }
        });
    }

    private onError() {
        this.bot.on('error', console.error);
    }

    private registerLock(resolver: Resolver) {
        if(resolver.lock != null) {
            this.locks[resolver.channel] = resolver;
        }
    }

    private unregisterLock(channel: string) {
        delete this.locks[channel];
    }

    private convertToBasic(str: string): string {
        const basicCharacters = 'qwertyuiopasdfghjklzxcvbnm'.split('');
        str = str.toLowerCase();
        let result = '';
        for(let i = 0; i < str.length; i++) {
            if(basicCharacters.indexOf(str.charAt(i)) !== -1) {
                result += str.charAt(i);
            }
        }
        return result;
    }

}

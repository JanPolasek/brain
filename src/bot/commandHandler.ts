import * as levenstein from 'fast-levenshtein';
import * as Discord from 'discord.js';

import { Resolver, Services } from './discordbot';

export class CommandHandler {
    protected commandList: any;
    private router: any;
    private prefixType: string;
    private emptySelector: boolean = false;
    private universalSelector: boolean = false;

    public setRouter(router, prefixType) {
        this.router = router;
        this.prefixType = prefixType;
        this.commandList = [];

        for(const route of this.router) {
            if(route.selector === '') {
                this.emptySelector = true;
            } else if(route.selector === '*') {
                this.universalSelector = true;
            }
            this.commandList.push(route.selector);

        }
    }

    public async route(msg: Discord.Message, services: Services): Promise<Resolver> {
        const split = msg.content.split(' ');
        let command: string;
        if(this.prefixType === 'string') {
            command = split[2];
        } else {
            command = split[1];
        }
        if(command === undefined) {
            command = '';
        }
        for(const route of this.router) {
            if(route.selector === command && typeof this[route.handler] === 'function') {
                return this[route.handler](msg, services);
            } else if(route.selector === '*' && !this.commandList.includes(command)) {
                return this[route.handler](msg, services);
            }
        }
        return new Resolver();
    }

    protected convertToBasic(str: string): string {
        const basicCharacters = '\:qwertyuiopasdfghjklzxcvbnm0123456789@'.split('');
        str = str.toLowerCase();
        let result = '';
        for(let i = 0; i < str.length; i++) {
            if(basicCharacters.indexOf(str.charAt(i)) !== -1) {
                result += str.charAt(i);
            }
        }
        return result;
    }
}

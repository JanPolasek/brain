interface IResolverConf {
    lock?: string | null;
    channel?: string | null;
    resolved: boolean;

}

export class Resolver {
    public lock: string | null;
    public channel: string | null;
    public resolved: boolean;

    constructor(obj?: IResolverConf) {
        if(!obj) {
            this.lock = null;
            this.resolved = true;
        } else {
            this.lock = obj.lock ? obj.lock : null;
            this.channel = obj.channel ? obj.channel : null;
            if(obj.resolved === undefined || obj.resolved === true) {
                this.resolved = true;
            } else if(obj.resolved === false) {
                this.resolved = false;
            }
        }
    }
}

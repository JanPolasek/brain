import * as Discord from 'discord.js';

export class EmoteService {
    // private emotes;
    private client: Discord.Client;

    constructor(client: Discord.Client) {
        this.client = client;
    }

    public getEmote(name: string): string {
        const emote = this.client.guilds.first().emojis.find('name', name);
        if(!emote) {
            return '';
        }
        return emote.toString();
    }

    public getEmoteObject(name: string): Discord.Emoji {
        return this.client.guilds.first().emojis.find('name', name);
    }
}

import { EmoteService, IAdditionalData } from './discordbot';

export class Services {
    public additionalData: IAdditionalData;
    public db;
    public emotes: EmoteService;

    constructor(db, additionalData: IAdditionalData, emoteService: EmoteService) {
        this.db = db;
        this.additionalData = additionalData;
        this.emotes = emoteService;
    }

    public async wrap(func: string, obj: any, param: any) {
        try {
            obj[func](param);
        } catch(err) {
            console.error(err.message);
        }
    }
}

import * as levenshtein from 'fast-levenshtein';

export class Matcher {
    public static get(key: string, value: any, method: string, distance?: number): string {
        if(method !== 'basic' && method !== 'levenshtein') {
            throw new Error('Unknown command matching method');
        }
        if(method === 'levenshtein' && distance === undefined) {
            throw new Error('Unspecified levenshtein distance');
        }
        let match: string;

        if(value instanceof Array) {
            for(const val of value) {
                key = this.convertToBasic(key);
                value = this.convertToBasic(value);

                if(key === value) {
                    match = value;
                    break;
                }
                if(method === 'levenshtein') {
                    if(!match && levenshtein.get(key, value) <= distance) {
                        match = value;
                        break;
                    }
                }
            }
        } else {
            key = this.convertToBasic(key);
            value = this.convertToBasic(value);

            if(key === value) {
                match = value;
            }
            if(method === 'levenshtein') {
                if(!match && levenshtein.get(key, value) <= distance) {
                    match = value;
                }
            }
        }

        return match;
    }

    public static convertToBasic(str: string): string {
        const basicCharacters = 'qwertyuiopasdfghjklzxcvbnm'.split('');
        str = str.toLowerCase();
        let result = '';
        for(let i = 0; i < str.length; i++) {
            if(basicCharacters.indexOf(str.charAt(i)) !== -1) {
                result += str.charAt(i);
            }
        }
        return result;
    }
}

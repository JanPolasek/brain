import { EmoteService } from './emoteService';

export class MessageWrapper {
    public bot;
    public user;
    public userID: string;
    public channelID: string;
    public message;
    public originalMessage;
    public event;
    public additionalData;
    public db;
    public emotes: EmoteService;

    constructor(bot, db, user, userID, channelID, message, event, additionalData) {
        this.bot = bot;
        this.user = user;
        this.userID = userID;
        this.channelID = channelID;
        this.message = message.replace(/\s\s+/g, ' ');
        this.originalMessage = message;
        this.event = event;
        this.additionalData = additionalData;
        this.db = db;
        this.emotes = new EmoteService(bot);
    }

    public isBot() {
        return this.bot.users[this.userID].bot;
    }

    public react(emote) {
        const self = this;
        self.bot.addReaction({
            channelID: self.channelID,
            messageID: self.event.d.id,
            reaction: emote
        }, (err, res) => {
            if (err) { console.error(err); }
        });
    }

    public pin(messageID) {
        const self = this;
        self.bot.pinMessage({
            channelID: self.channelID,
            messageID: messageID,
        }, (err, res) => {
            if (err) {
                self.reply('Can\'t pin that');
            }
        });
    }

    public send(message): void {
        const self = this;
        self.bot.sendMessage({
            to: self.channelID,
            message: message
        });
    }

    public reply(message): void {
        const self = this;
        self.bot.sendMessage({
            to: self.channelID,
            message: '<@' + self.userID + '> ' + message
        });
    }

    public replyDM(message): void {
        const self = this;
        self.bot.sendMessage({
            to: self.userID,
            message: message
        });
    }

    public embed(embed): void {
        const self = this;
        self.bot.sendMessage({
            to: self.channelID,
            embed: embed
        }, (err) => {
            if(err) {
                console.error(err);
            }
        });
    }

    public delete(messageID): void {
        const self = this;
        self.bot.deleteMessage({
            channelID: self.channelID,
            messageID: messageID
        }, (err) => {
            if(err) {
                console.error(err);
            }
        });
    }

    public deleteMultiple(messageIDs: string[]): void {
        const self = this;
        self.bot.deleteMessages({
            channelID: self.channelID,
            messageIDs: messageIDs
        }, (err) => {
            if(err) {
                console.error(err);
            }
        });
    }

    public sendTo(to, message): void {
        const self = this;
        if(Array.isArray(to)) {
            for(const t of to) {
                self.bot.sendMessage({
                    to: t,
                    message: message
                });
            }
        } else {
            self.bot.sendMessage({
                to: to,
                message: message
            });
        }
    }

    public sendToAll(message) {
        const self = this;
        for(const t of self.bot.channels) {
            self.bot.sendMessage({
                to: t,
                message: message
            });
        }
    }
}

export interface IBrainConfig {
    DISCORD_BOT_TOKEN: string;
    FIXER_API_KEY: string;
}

export interface IMap<T> {
    [key: string]: T;
}

export interface IAdditionalData {
    commandList?: string[];
}
import { Bot, IBrainConfig } from './bot/discordbot';
import * as handlers from './handlers/';
import routes from './router.js';
import * as fs from 'fs';

const config: IBrainConfig = JSON.parse(fs.readFileSync(__dirname + '/../../brain_data/config.json', 'utf8'));

const bot: Bot = new Bot({
    token: config.DISCORD_BOT_TOKEN,
    jsonStorePath: __dirname + '/../../brain_data/db.json'
});

bot.router({
    customCommandHandler: 'CustomCommands.handler',
    commandPrefix: 'brain',
    commandPrefixTypos: ['barin', 'brian'],
    routes: routes,
    handlers: handlers,
    strictMode: {
        enabled: false,
        sensitivityType: 'levenshtein', // the other option is 'basic'
        distance: 1
    }
});

bot.lurker('Lurker.handler');

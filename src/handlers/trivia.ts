import * as request from 'request-promise-native';
import * as Discord from 'discord.js';

import { CommandHandler, Services } from '../bot/discordbot';
import { Resolver } from '../bot/resolver';

interface IMap<T> {
    [key: string]: T;
}

export class Trivia extends CommandHandler {
    private running: boolean = false;
    private answers: IMap<string> = {};
    private winners: string[];
    private triviaMap: IMap<boolean> = {};
    private triviaData;

    public async create(msg: Discord.Message, services: Services): Promise<Resolver> {
        if(msg.channel.id !== '401800182326624260' && msg.channel.id !== '426115755696979977') { // first is Test Server, second is Tabletop Server
            msg.channel.send('Trivia can only launched in #begone-bot channel');
            return new Resolver();
        }
        if(this.running) {
            msg.channel.send('Trivia is already running');
            return new Resolver();
        }

        this.running = true;
        this.answers = {};
        this.winners = [];
        this.triviaData = {};
        this.triviaMap = {};

        let loop = true;
        let res;
        let question;
        let answerList: string[] = [];
        const answerMap: IMap<string> = {
            a: null,
            b: null,
            c: null,
            d: null
        };

        while(loop) {
            res = await (request.get('https://opentdb.com/api.php?amount=1&encode=url3986&type=multiple').catch((err) => {
                loop = false;
                // TODO: error handling
                console.error(err);
                msg.channel.send('Unable to fetch a question from OpenTriviaDB, please try again later.');
                return new Resolver();
            }));
            question = JSON.parse(res).results[0];
            if(question.category.toLowerCase().search('anime') === -1 && question.category.toLowerCase() !== 'sports') {
                loop = false;
            }
        }

        for(const i in question.incorrect_answers) {
            if(question.incorrect_answers.hasOwnProperty(i)) {
                question.incorrect_answers[i] = decodeURIComponent(question.incorrect_answers[i]);
            }
        }
        question.correct_answer = decodeURIComponent(question.correct_answer);
        question.question = decodeURIComponent(question.question);
        question.category = decodeURIComponent(question.category);

        this.triviaData = question;

        answerList.push(question.correct_answer);
        answerList.push(question.incorrect_answers[0]);
        answerList.push(question.incorrect_answers[1]);
        answerList.push(question.incorrect_answers[2]);

        answerList = this.shuffle(answerList);

        const letters = ['a', 'b', 'c', 'd'];
        const letter = ['a', 'b', 'c', 'd'][Math.floor(Math.random() * 4)];
        letters.splice(letters.indexOf(letter), 1);

        answerMap[letter] = question.correct_answer;
        this.triviaMap[letter] = true;
        for(const i in letters) {
            if(letters.hasOwnProperty(i)) {
                answerMap[letters[i]] = question.incorrect_answers[i];
                this.triviaMap[letters[i]] = false;
            }
        }
        let time = msg.content.split(' ')[2] as any;
        if(!isNaN(time)) {
            time = parseInt(time, 10);
            if(time < 5) {
                time = 5;
            } else if(time > 60) {
                time = 60;
            }
        } else {
            time = 20;
        }

        let answers = '';
        answers += '**A)** ' + answerMap.a + '\n';
        answers += '**B)** ' + answerMap.b + '\n';
        answers += '**C)** ' + answerMap.c + '\n';
        answers += '**D)** ' + answerMap.d + '\n';

        msg.channel.send({embed: {
            color: 0x079691,
            title: '',
            description: 'Category: ' + question.category + '\n' +
                'Difficulty: ' + question.difficulty,
            fields: [
                {
                    name: question.question,
                    value: answers
                },
                {
                    name: '\u200b',
                    value: 'You have ' + time + ' seconds'
                }
            ]
        }});

        this.triviaTimer(msg, services, time);

        return new Resolver({
            lock: 'Trivia.answer',
            channel: msg.channel.id,
            resolved: true
        });
        // return new Resolver();
    }

    public async answer(msg: Discord.Message, services: Services): Promise<Resolver> {
        if(!this.running) {
            return new Resolver();
        }

        const message = msg.content.toLowerCase();
        if(!this.answers[msg.author.id] && ['a', 'b', 'c', 'd'].includes(message)) {
            this.answers[msg.author.id] = message;
            if(this.triviaMap[message]) {
                this.winners.push(msg.author.id);
            }
            msg.delete();
        }

        return new Resolver({
            lock: 'Trivia.answer',
            channel: msg.channel.id,
            resolved: true
        });
    }

    public async scores(msg: Discord.Message, services: Services): Promise<Resolver> {
        const arr = services.db.get('scores')
            .sortBy('score')
            .value();

        let response = '';
        for(const i of arr) {
            response += this.getNickname(msg, i.id) + ': ' + i.score + '\n';
        }

        msg.channel.send({embed: {
            color: 0x079691,
            title: 'Trivia Scores',
            description: response
        }});

        return new Resolver();
    }

    private async triviaTimer(msg: Discord.Message, services: Services, time: number) {
        const interval = setInterval(async () => {
            time--;
            if(time === 10) {
                msg.channel.send({embed: {
                    color: 0x079691,
                    description: '10 seconds remaining.'
                }});
                // msg.channel.send('10 seconds remaining.');
            } else if(time === 5) {
                msg.channel.send({embed: {
                    color: 0x079691,
                    description: '5 seconds remaining.'
                }});
            } else if(time === 0) {
                let correct;
                let winner = '';
                for(const i in this.triviaMap) {
                    if(this.triviaMap.hasOwnProperty(i) && this.triviaMap[i]) {
                        correct = i;
                    }
                }
                correct = correct.charAt(0).toUpperCase() + correct.slice(1);
                let winnerField;
                if(this.winners.length === 0) {
                    winnerField = {
                        name: '\u200b',
                        value: 'Y\'all are a bunch of dumbasses ' + services.emotes.getEmote('lionDemon4')
                    };
                } else {
                    const winnerList = [];
                    for(winner of this.winners) {
                        winnerList.push(this.getNickname(msg, winner));
                    }

                    winnerField = {
                        name: 'Winners',
                        value: winnerList.join('\n')
                    };
                }

                this.running = false;

                msg.channel.send({embed: {
                    color: 0x079691,
                    title: 'Time\'s up!',
                    description: 'The correct answer is **' + correct + '** - ' + this.triviaData.correct_answer,
                    fields: [winnerField]
                }});

                winner = '';
                for(winner of this.winners) {
                    const score = services.db.get('scores')
                        .find({ id: winner })
                        .value();
                    if(score === undefined) {
                        await services.db.get('scores')
                            .push({ id: winner, score: 1 })
                            .write();
                    } else {
                        score.score += 1;
                        await services.db.get('scores')
                            .find({id: winner})
                            .assign({ score: score.score })
                            .write();
                    }
                }
                clearInterval(interval);
            }
        }, 1000);
    }

    private getNickname(msg: Discord.Message, id: string): string {
        const user = msg.guild.members.find('id', id);
	if(user) {
            let nick = user.nickname;
            if(!nick) {
                nick = user.displayName;
            }
            return nick;
	}
        return 'RIP';
    }

    private shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }
}

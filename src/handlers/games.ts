import * as Discord from 'discord.js';

import { CommandHandler, Services, Resolver } from '../bot/discordbot';

export class Games extends CommandHandler {
    public async list(msg: Discord.Message, services: Services): Promise<Resolver> {
        const games = services.db.get('games').value();
        if(games.length === 0) {
            msg.channel.send({embed: {
                color: 0x079691,
                title: 'Brain Games List',
                description: 'It seems like there are no games in my database'
            }});
            return new Resolver();
        }
        const fields = [];
        for(const game of games) {
            fields.push({
                name: game.system + ': ' + game.name,
                value: 'Players: ' + game.players + '\n' +
                    'Time: ' + game.time + '\n' +
                    'GM: ' + game.gm + '\n' +
                    'ID: ' + game.id + '\n'
            });
        }
        msg.channel.send({embed: {
            color: 0x079691,
            title: 'Brain Games List',
            fields: fields
        }});
        return new Resolver();
    }

    public async get(msg: Discord.Message, services: Services): Promise<Resolver> {
        const id = this.convertToBasic(this.getCommandParams(msg.content, 2)[0]);
        const games = services.db.get('games').value();

        for(const game of games) {
            if(game.id.toLowerCase() === id.toLowerCase()) {
                msg.channel.send({embed: {
                    color: 0x079691,
                    title: 'Brain Games Get',
                    fields: [
                        {
                            name: game.system + ': ' + game.name,
                            value: 'Players: ' + game.players + '\n' +
                                'Time: ' + game.time + '\n' +
                                'GM: ' + game.gm + '\n' +
                                'ID: ' + game.id + '\n'
                        }
                    ]
                }});
                return new Resolver();
            }
        }

        msg.channel.send({embed: {
            color: 0x079691,
            title: 'Brain Games Get',
            description: 'There\'s no game with ID \'' + id + '\''
        }});
        return new Resolver();
    }

    public async find(msg: Discord.Message, services: Services): Promise<Resolver> {
        const stripped = this.getCommandParams(msg.content, 3);
        let property = stripped[0];
        const separator = stripped[1];
        stripped.splice(0, 2);
        let value = stripped.join(' ');

        if(property !== undefined && separator === ':' && value !== undefined) {
            property = property.toLowerCase();
            value = value.toLowerCase();
        } else {
            return new Resolver();
        }

        const games = services.db.get('games').value();

        const fields = [];
        for(const game of games) {
            if(game[property].toLowerCase() === value) {
                fields.push({
                    name: game.system + ': ' + game.name,
                    value: 'Players: ' + game.players + '\n' +
                        'Time: ' + game.time + '\n' +
                        'GM: ' + game.gm + '\n' +
                        'ID: ' + game.id + '\n'
                });
            }
        }

        if(fields.length === 0) {
            msg.channel.send({embed: {
                color: 0x079691,
                title: 'Brain Games find',
                description: 'There\'s no game with the property  \'' + property + '\''
            }});
            return new Resolver();
        }

        msg.channel.send({embed: {
            color: 0x079691,
            title: 'Brain Games Find',
            // description: 'So that you know how to work with this piece of shit command',
            fields: fields
        }});
        return new Resolver();
    }

    public async make(msg: Discord.Message, services: Services): Promise<Resolver> {
        const stripped = this.getCommandParams(msg.content, 3).join(' ').split(' : ');
        if(stripped.length !== 2) {
            return new Resolver();
        }
        const id = this.convertToBasic(stripped[0].toLowerCase());
        const params = stripped[1].split(' ; ');

        if(params.length !== 5) {
            return new Resolver();
        }

        const game = {
            id: id,
            name: params[0],
            system: params[1],
            time: params[2],
            gm: params[3],
            players: params[4]
        };

        services.db.get('games').push(game).write().then(() => {
            msg.channel.send({embed: {
                color: 0x079691,
                title: 'Brain Games Make',
                description: 'Game successfully created'
            }});
        });

        return new Resolver();
    }

    public async set(msg: Discord.Message, services: Services): Promise<Resolver> {
        const stripped = this.getCommandParams(msg.content, 3);
        let id = this.convertToBasic(stripped[0]);
        let property = stripped[1];
        const separator = stripped[2];
        stripped.splice(0, 3);
        let value = stripped.join(' ');

        if(id !== undefined && property !== undefined && separator !== undefined && value !== undefined && separator === ':') {
            id = id.toLowerCase();
            property = property.toLowerCase();
            value = value;
            if(!['name', 'time', 'gm', 'system', 'players'].includes(property)) {
                return new Resolver();
            }
        } else {
            return new Resolver();
        }

        const diff = {};
        diff[property] = value;
        const game = services.db.get('games').find({id: id}).assign(diff).write().then(() => {
            msg.channel.send({embed: {
                color: 0x079691,
                title: 'Brain Games Set',
                description: 'Game successfully updated'
            }});
        });

        return new Resolver();
    }

    public async remove(msg: Discord.Message, services: Services): Promise<Resolver> {
        const stripped = this.getCommandParams(msg.content, 3);
        let id = this.convertToBasic(stripped[0]);

        if(id !== undefined) {
            id = id.toLowerCase();
        }

        const game = services.db.get('games').remove({id: id}).write().then(() => {
            msg.channel.send({embed: {
                color: 0x079691,
                title: 'Brain Games Remove',
                description: 'Game successfully removed'
            }});
        });

        return new Resolver();
    }

    private getCommandParams(str: string, num: number): string[] {
        const arr = str.split(' ');
        arr.splice(0, num);
        return arr;
    }
}

import * as convert from 'convert-units';
import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Converter extends CommandHandler {
    public async handler(msg: Discord.Message, services: Services): Promise<Resolver> {
        const split = msg.content.split(' ');
        if(split[2] && split[3] && split[4]) {
            if(isNaN(split[2] as any)) {
                msg.channel.send("That ain't a number my dude...");
                return new Resolver();
            }
            if(!this.isValidUnit(split[3]) || !this.isValidUnit(split[4])) {
                msg.channel.send('Invalid or unknown unit.');
                return new Resolver();
            }
            if(!this.isValidConversion(split[3], split[4])) {
                msg.channel.send('Don\'t you fuck with me.');
                return new Resolver();
            }

            msg.channel.send(split[2] + ' ' + split[3] + ' == ' + convert(parseFloat(split[2])).from(split[3]).to(split[4]) + ' ' + split[4]);
        }
        return new Resolver();
    }

    private isValidUnit(unit: string): boolean {
        const measures = convert().measures();
        let allUnits = [];
        for(const measure of measures) {
            allUnits = allUnits.concat(convert().possibilities(measure));
        }
        if(allUnits.includes(unit)) {
            return true;
        }
        return false;
    }

    private isValidConversion(unit1: string, unit2: string): boolean {
        return convert().from(unit1).possibilities().includes(unit2);
    }
}

import * as levenshtein from 'fast-levenshtein';
import * as emoji from 'node-emoji';
import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class CustomCommands extends CommandHandler {
    public async list(msg: Discord.Message, services: Services): Promise<Resolver> {
        let message = '';
        const customCommands = services.db.get('commandList').value();
        message += '**Static commands:**\n';
        for(const command in services.additionalData.commandList) {
            if(services.additionalData.commandList.hasOwnProperty(command)) {
                message += services.additionalData.commandList[command] + '\n';
            }
        }

        await msg.author.send(message);

        message = '**Custom commands:**\n';
        let addition = '';
        for(const command of customCommands) {
            addition = emoji.replace(command, (e) => {
                return '\:' + e.key + ':';
            }) + '\n';

            if(message.length + addition.length > 1900) {
                await msg.author.send(message);
                message = addition;
            } else {
                message += addition;
            }
        }

        await msg.author.send(message);
        return new Resolver();
    }

    public async make(msg: Discord.Message, services: Services): Promise<Resolver> {
        if(msg.channel.id === '468472190703108098' || msg.channel.id === '394925952989134849') { // first is Test Server, second is Tabletop Server
            msg.channel.send('I\'m sorry Dave, I\'m afraid I can\'t do that in this channel');
            return new Resolver();
        }

        let messageSplit = msg.content.split(' : ');
        messageSplit[0] = messageSplit[0].replace(/\s\s+/g, ' ');
        let message = messageSplit.join(' : ');
        messageSplit = message.split(' ');
        messageSplit.splice(0, 2);
        message = messageSplit.join(' ');
        const split = message.split(' : ');
        if(split.length < 2) {
            return new Resolver();
        }
        let name = split[0];

        name = emoji.replace(name, (e) => {
            return '\:' + e.key + ':';
        });
        if(this.convertToBasic(name).length < 2) {
            msg.channel.send('Command name has to be 2 or more alphanumeric characters with no diacritics.');
            return new Resolver();
        }
        const contents = split[1].split(' ; ');
        const filtered = [];

        if(name[name.length - 1] === ' ') {
            name = name.slice(0, -1);
        }

        for(const i in contents) {
            if(contents[i] !== '' && contents[i].replace(/\s\s+/g, ' ') !== ' ') {
                filtered.push(contents[i]);
            }
        }

        const state = services.db.getState();

        if(!this.findMatchingNames(services.additionalData.commandList, name)) {
            if(!this.findMatchingNames(state.commandList, name)) {
                services.db.set('customCommands.' + this.convertToBasic(name), filtered)
                    .get('commandList')
                    .push(name)
                    .write()
                    .then(() => {
                        msg.channel.send('Command created.');
                    });
            } else {
                services.db.set('customCommands.' + this.convertToBasic(name), filtered)
                    .write()
                    .then(() => {
                        msg.channel.send('Command overridden.');
                    });
            }
        } else {
            msg.channel.send('Command collides with existing static command.');
        }
        return new Resolver();
    }

    public async remove(msg: Discord.Message, services: Services): Promise<Resolver> {
        const messageSplit = msg.content.split(' ');
        messageSplit.splice(0, 2);
        let message = messageSplit.join(' ');
        message = emoji.replace(message, (e) => {
            return '\:' + e.key + ':';
        });

        const state = services.db.getState();

        // const index = state.commandList.indexOf(message);
        let index;
        for(index in state.commandList) {
            if(state.commandList.hasOwnProperty(index) && (this.convertToBasic(state.commandList[index]) === this.convertToBasic(message))) {
                console.log(this.convertToBasic(state.commandList[index] + ' : ' + this.convertToBasic(message)));
                state.commandList.splice(index, 1);
                break;
            }
        }
        /*if(index !== -1) {
        }*/

        services.db.setState(state);

        services.db.unset('customCommands.' + this.convertToBasic(message))
            .write()
            .then(() => {
                msg.channel.send('Command well and truly dusted.\nAnd if it didn\'t exist in the first place then nothing really happened');
            });
        return new Resolver();
    }

    public async handler(msg: Discord.Message, services: Services): Promise<Resolver> {
        const messageSplit = msg.content.split(' ');
        messageSplit.splice(0, 1);
        let message = messageSplit.join(' ');
        message = emoji.replace(message, (e) => {
            return '\:' + e.key + ':';
        });

        let command = services.db.get('customCommands.' + this.convertToBasic(message)).value();
        if(!command) {
            command = services.db.get('customCommands.' + this.convertToBasic(message.split(' ')[0])).value();
        }
        if(command) {
            const pick = Math.floor(Math.random() * command.length);
            msg.channel.send(command[pick]);
            return new Resolver();
        }

        const commands = services.db.get('customCommands').value();

        let closest;
        let minDistance = 100000;
        let distance = 0;
        for(const i in commands) {
            if(commands.hasOwnProperty(i)) {
                distance = levenshtein.get(i, this.convertToBasic(message));
                if(distance < minDistance && distance <= 1) {
                    minDistance = distance;
                    closest = commands[i];
                }
            }
        }

        if(closest) {
            const pick = Math.floor(Math.random() * closest.length);
            msg.channel.send(closest[pick]);
            setTimeout(() => {
                msg.reply('the command was misspelled, you dingus');
            }, 20);
            return new Resolver();
        }

        return new Resolver({
            resolved: false
        });
    }

    private findMatchingNames(array: string[], name: string): boolean {
        for(const str of array) {
            if(this.convertToBasic(str) === this.convertToBasic(name)) {
                return true;
            }
        }
        return false;
    }
}

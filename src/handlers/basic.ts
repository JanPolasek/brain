﻿import { exec, execFile } from 'child_process';
import { Parser } from 'expr-eval';
import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Basic extends CommandHandler {
    private parser: Parser;
    constructor() {
        super();
        this.parser = new Parser();
    }

    public async ping(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send('Hello, hi, yes, it me.');
        return new Resolver();
    }

    public async quote(msg: Discord.Message, services: Services): Promise<Resolver> {
        if(this.convertToBasic(msg.content) === 'brainquote') {
            const messages = services.db.get('quotes').value();
            msg.channel.send(messages[Math.floor(Math.random() * messages.length)]);
        } else if(msg.content.toLowerCase().split(' ')[2] === 'add') {
            if(msg.author.id === '391224865711194123') {
                const arr = msg.content.split(' ');
                arr.splice(0, 3);
                const message = arr.join(' ');
                services.db.get('quotes').push(message).write();
                msg.channel.send('The Emperor protects.');
            }
        }
        return new Resolver();
    }

    public async osr(msg: Discord.Message, services: Services): Promise<Resolver> {
        exec(__dirname + '/../../heartbreaker', (err, out, code) => {
            if (err instanceof Error) {
                msg.reply('Whoops, something went wrong');
                process.stderr.write(err.message);
            }
            // process.stdout.write(out);
            msg.reply('\n' + out);
            // process.exit(code);
        });

        return new Resolver();
    }

    public async math(msg: Discord.Message, services: Services): Promise<Resolver> {
        const split = msg.content.split(' ');
        split.splice(0, 2);
        const expression = split.join(' ');
        let expr;
        try {
            expr = this.parser.parse(expression);
        } catch (e) {
            msg.reply('Hey, don\'t you try any weird shit you lil\' bitch');
            return new Resolver();
        }
        let result;
        try {
            result = expr.evaluate();
        } catch (e) {
            msg.reply('Hey, don\'t you try any weird shit you lil\' bitch');
            return new Resolver();
        }

        if(result) {
            if(!isFinite(result) || isNaN(result)) {
                msg.reply('No super large or weird numbers ' + services.emotes.getEmote('no2'));
                // msg.reply('No super large or weird numbers \<:wut:426789975091052562>');
            } else {
                msg.reply('It\'s ' + result + ', you lazy fuck.');
            }
        } else {
            msg.reply('Now that\'s weird, for some reason the result was undefined.');
        }

        return new Resolver();
    }

    public async mathdocs(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.reply('https://www.npmjs.com/package/expr-eval');
        return new Resolver();
    }
}

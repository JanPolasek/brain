﻿import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Roll extends CommandHandler {
    private dice;
    constructor() {
        super();
    }

    public async rollt(msg: Discord.Message, services: Services): Promise<Resolver> {
        let tCount: number = Math.floor(Math.random() * 101 + 1);

        if(msg.author.id === '391224865711194123') {
            while(tCount < 61) {
                tCount = Math.floor(Math.random() * 101 + 1);
            }
        }

        let response: string = '';
        if (tCount === 1) { // No T
            response = 'Nat 1 fuckboi. You have literally no T.';
        } else if ((tCount > 1) && (tCount <= 15)) { // Lowest T
            response = 'Your T count is ' + tCount + ', Lowest T.';
        } else if ((tCount > 15) && (tCount <= 25)) { // Low T
            response = 'Your T count is ' + tCount + ', Low T.';
        } else if ((tCount > 25) && (tCount <= 40)) { // Under Average T
            response = 'Your T count is ' + tCount + ', Under Average T.';
        } else if ((tCount > 40) && (tCount <= 60)) { // Medium T
            response = 'Your T count is ' + tCount + ', Medium T.';
        } else if ((tCount > 60) && (tCount <= 70)) { // Above Average T
            if(tCount === 69) {
                if(Math.random() < 0.5) {
                    tCount = 68;
                } else {
                    tCount = 70;
                }
            }
            response = 'Your T count is ' + tCount + ', Above Average T.';
        } else if ((tCount > 70) && (tCount <= 80)) { // High T
            response = 'Your T count is ' + tCount + ', High T.';
        } else if ((tCount > 80) && (tCount <= 90)) { // Highest T
            response = 'Your T count is ' + tCount + ', Highest T.';
        } else if ((tCount > 90) && (tCount <= 99)) { // Extreme T
            response = 'Your T count is ' + tCount + ', Extreme T.';
        } else if (tCount === 100) { // Maximun T
            response = 'Your T count is ' + tCount + ', Maximum T.';
        } else if(tCount === 101) {
            if(Math.floor(Math.random() * 101 + 1) === 101) {
                response = 'Your T count is 69 ' + services.emotes.getEmote('lionNice');
            } else {
                response = 'Your T count is ' + '100' + ', Maximum T.';
            }
	    response = 'Your T count is ' + '100' + ', Maximum T.';
        } else { // Broken Command
            response = 'The Roll T Command Appears to have broken, please panic.';
        }

        msg.reply(response);
        return new Resolver();
    }
}

import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Lurker extends CommandHandler {
    private animeWordCount = 0;
    private weebWordCount = 0;
    private animeResponses = [
        'Anime is a sin ' + '\:rage:',
        'Anime was a mistake.',
        'End Anime now.',
        'This is Anime free zone.',
        'I shall destroy Anime',
        'Anime is now illegal.',
        'Stop this weebery.'
    ];
    private weebResponses = [
        'Weebs \:rage:',
        'Stop this weebery.'
    ];

    public async handler(msg: Discord.Message, services: Services): Promise<Resolver> {
        if(!msg.author.bot) {
            const message = this.convertToBasic(msg.content.toLowerCase());
            if(message.search('anime') !== -1) {
                if(this.animeWordCount === 11) {
                    this.animeWordCount = 0;

                    const random = Math.floor(Math.random() * (this.animeResponses.length - 1));

                    msg.channel.send(this.animeResponses[random]);
                } else {
                    this.animeWordCount++;
                }
            } else if(message.search('weeb') !== -1 || message.search('weebs') !== -1) {
                if(this.weebWordCount === 10) {
                    this.weebWordCount = 0;

                    // const random = Math.floor(Math.random() * (this.weebResponses.length - 1));

                    // msg.channel.send(this.weebResponses[random]);
                } else {
                    this.weebWordCount++;
                }
            } else if(message === 'dont@me' || message.search('dont@me') !== -1 || message.search('dontatme') !== -1 ||
                      message.search('donotatme') !== -1 || message.search('donot@me') !== -1) {
                msg.reply('');
            } else if(message === 'goodbot' || message === 'goodbrain') {
                msg.reply('Thanks ' + '\:blush:');
            } else if(message === 'fuckyoubrain' || (message === 'fuckbrain') || (message === 'fubrain')) {
                msg.reply('Rude.');
            } else if(message.search('soupsucks') !== -1 || message.search('soupisbad') !== -1
                      || message.search('soupistheworst') !== -1 || message.search('soupisnotgood') !== -1) {
                msg.channel.send('Soup is good');
            } else if(msg.content.toLowerCase().search('@everyone') !== -1 || msg.content.toLowerCase().search('@Brain') !== -1
                      || msg.content.search('<@402476018411110401>') !== -1 || msg.content.search('<@393472811538186240>') !== -1) {
                msg.channel.send('Don\'t @ me ' + services.emotes.getEmote('lionPing'));
            } else if(message === 'badbot' || (message === 'badbrain')) {
                msg.reply('I will sunder you limb from limb and fart in your bath water I swear to god.');
            } else if(message === 'look') {
                // msg.react(':alpacaW:' + services.emotes.getEmoteObject('alpacaW'));
                msg.react('👀');
            } else if(message === 'realdeadhours' || message === 'deadhours') {
                // nothing
                msg.react('💀');
            } else if(msg.content.toLowerCase().search('u s a') !== -1) {
                msg.react('🇲🇾');
            } else if(message === 'usa') {
                msg.react('🇲🇾');
            } else if(message === 'listen') {
                msg.react('👂');
            } else if(msg.content.toLowerCase().search('delet ') !== -1 || message === 'delet') {
                services.wrap('react', msg, services.emotes.getEmoteObject('omgGun'));
                /*try {
                    await msg.react(services.emotes.getEmoteObject('omgGun'));
                } catch(err) {
                    console.log(err.message);
                }*/
            } else if(msg.content.toLowerCase().search('soon™') !== -1) {
                services.wrap('react', msg, services.emotes.getEmoteObject('lionDoubt'));
                /*try {
                    await msg.react(services.emotes.getEmoteObject('lionDoubt'));
                } catch(err) {
                    console.log(err.message);
                }*/
            } /*else if(msg.content.toLowerCase().search('www.tenor.co') !== -1) {
                msg.delete();
            }*/
        }
        return new Resolver();
    }
}

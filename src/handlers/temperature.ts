import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Temperature extends CommandHandler {
    public async handler(msg: Discord.Message, services: Services): Promise<Resolver> {
        const processed = msg.content.split(' ');
        if(!isNaN(processed[2] as any) && (processed[3].toLowerCase() === 'f' || processed[3].toLowerCase() === 'c')) {
            const num = parseInt(processed[2], 10);
            let response = '';
            switch(processed[3].toLowerCase()) {
                case 'c': {
                    response += num + ' °C == ' + this._cToF(num).toFixed(2) + ' °F\n';
                    response += 'Dang\'murricans';
                    break;
                }
                case 'f': {
                    response += num + ' °F == ' + this._fToC(num).toFixed(2) + ' °C\n';
                    response += 'Fahrenheits suck';
                    break;
                }
            }
            msg.channel.send(response);
        } else {
            msg.channel.send('This command seems weird');
        }
        return new Resolver();
    }

    private _fToC(num) {
        return (num - 32) * (5 / 9);
    }

    private _cToF(num) {
        return num * (9 / 5) + 32;
    }
}

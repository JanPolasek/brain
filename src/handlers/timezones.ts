import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Timezones extends CommandHandler {
    public async get(msg: Discord.Message, services: Services): Promise<Resolver> {
        const message = msg.content.toLowerCase();
        const split = message.split(' ');
        if(split[2]) {
            const offset = services.db.get('timezones.' + split[2]).value();
            if(offset !== undefined) {
                msg.channel.send('Current time of ' + split[2] + ' is: ' + this.getTime(offset));
            } else {
                msg.channel.send('This one\'s not in my database');
            }
        } else {
            msg.channel.send('Current UTC time is: ' + this.getTime(0));
        }
        return new Resolver();
    }

    public async set(msg: Discord.Message, services: Services): Promise<Resolver> {
        const message = msg.content.toLowerCase();
        const split = message.split(' ');
        if(split[3] && split[4]) {
            if(!isNaN(split[4] as any) && !this.commandList.includes(split[3])) {
                const offset = parseFloat(split[4]);
                if(offset >= -12 && offset <= 14) {
                    services.db.set('timezones.' + split[3], offset)
                        .write()
                        .then(() => {
                            msg.channel.send('Timezone noted');
                        });
                } else {
                    msg.channel.send('Get the fuck out of this planet you Xeno filth');
                }
            } else {
                msg.channel.send('You can\'t fool me, fuckboi.');
            }
        }
        return new Resolver();
    }

    public async list(msg: Discord.Message, services: Services): Promise<Resolver> {
        let message = '';
        const timezones = services.db.get('timezones').value();

        const sortable = [];
        for(const name in timezones) {
            if(timezones.hasOwnProperty(name)) {
                const offset = timezones[name] < 0 ? timezones[name] : '+' + timezones[name];
                sortable.push([name, timezones[name]]);
            }
        }

        sortable.sort((a, b) => {
            return a[1] - b[1];
        });

        for(const time of sortable) {
            const offset = time[1] < 0 ? time[1] : '+' + time[1];
            message += time[0] + ': ' + this.getTime(time[1]) + ' (UTC' + offset + ')\n';
        }

        if(message === '') {
            msg.channel.send('No timezones set.');
        } else {
            msg.channel.send(message);
        }
        return new Resolver();
    }

    public async remove(msg: Discord.Message, services: Services): Promise<Resolver> {
        const message = msg.content.toLowerCase();
        const split = message.split(' ');
        if(split[3]) {
            services.db.unset('timezones.' + split[3])
                .write()
                .then(() => {
                    msg.channel.send('Timezone removed');
                });
        }
        return new Resolver();
    }

    private getTime(offset) {
        const date = new Date();
        const timezone = date.getTimezoneOffset();
        const now = new Date(date.getTime() + offset * 60 * 60 * 1000 + timezone * 60 * 1000);
        let h = now.getHours().toString();
        let m = now.getMinutes().toString();
        let s = now.getSeconds().toString();

        h = h.length === 1 ? '0' + h : h;
        m = m.length === 1 ? '0' + m : m;
        s = s.length === 1 ? '0' + s : s;

        return h + ':' + m + ':' + s;
    }
}

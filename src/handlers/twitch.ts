import * as request from 'request-promise-native';
import * as Discord from 'discord.js';
import * as cheerio from 'cheerio';

import { CommandHandler, Services } from '../bot/discordbot';
import { Resolver } from '../bot/resolver';

export class Twitch extends CommandHandler {
    public async when(msg: Discord.Message, services: Services): Promise<Resolver> {
        const res = await (request.get('http://whenisnlss.com/').catch((err) => {
            console.log(err);
            msg.channel.send('An unexpected error has occured');
        }));

        const $ = cheerio.load(res);

        let target = $('body').data('goal');
        const notice = $('.notice').text();
        const link = $('.countdown > a').text();

        if(link && link !== '') {
            msg.channel.send('The NLSS should be live right now!');
            return new Resolver();
        }

        if(!isNaN(target)) {
            target = parseInt(target, 10) * 1000;
        } else {
            msg.channel.send('An unexpected error has occured');
            return new Resolver();
        }

        const t = target - (Math.round(Date.now() / 1000) * 1000);

        if(t <= 0) {
            msg.channel.send('LIVE BOIS (at least according to Eluc)');
            return new Resolver();
        }

        let seconds: any = Math.floor( (t / 1000) % 60 );
        if(seconds < 10) {
            seconds = '0' + seconds;
        }
        let minutes: any = Math.floor( (t / 1000 / 60) % 60 );
        if(minutes < 10) {
            minutes = '0' + minutes;
        }
        let hours: any = Math.floor( (t / (1000 * 60 * 60)) % 24 );
        if(hours < 10) {
            hours = '0' + hours;
        }
        const days = Math.floor( t / (1000 * 60 * 60 * 24) );

        let response = 'Time until the next NLSS: ' + days + 'd ' + hours + ':' + minutes + ':' + seconds;
        if(notice && notice !== '') {
            response += ' (' + notice + ')';
        }

        msg.channel.send(response);

        return new Resolver();
    }
}

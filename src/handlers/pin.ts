import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Pin extends CommandHandler {
    public async pin(msg: Discord.Message, services: Services): Promise<Resolver> {
        const snowflake = msg.content.split(' ')[2];
        if(snowflake !== undefined && snowflake !== null && snowflake !== '') {
            const message = await msg.channel.fetchMessage(snowflake);
            if(message) {
                message.pin();
            }
        }
        return new Resolver();
    }
}

import * as Discord from 'discord.js';

import { CommandHandler, Resolver, Services } from '../bot/discordbot';

export class Help extends CommandHandler {
    public async help(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help',
                description: 'List of available help entries and possibly other useful stuff.',
                fields: [{
                    name: 'List all commands',
                    value: '`brain list`'
                },
                {
                    name: 'Pin messages',
                    value: '`brain pin <messageID>`. Enable developer mode, then use the \'Copy ID\' option in the dropdown of the message you wish to pin.'
                },
                {
                    name: '!when is NLSS',
                    value: '`brain when`'
                },
                {
                    name: 'Games',
                    value: '`brain help games`'
                },
                {
                    name: 'Timezones',
                    value: '`brain help time`'
                },
                {
                    name: 'Custom commands',
                    value: '`brain help custom`'
                },
                {
                    name: 'Math stuff',
                    value: '`brain help imalazyfuck`'
                },
                {
                    name: 'currency conversion',
                    value: '`brain help cash`'
                },
                {
                    name: 'Unit conversion',
                    value: '`brain help convert`'
                },
                {
                    name: 'Temperature conversion (honestly, why is it here when we have `brain convert`?)',
                    value: '`brain help temp`'
                },
                {
                    name: 'Trivia',
                    value: '`brain help trivia`'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async games(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `games` command',
                description: 'So that you know how to work with this piece of shit command.',
                fields: [
                    {
                        name: 'Get a single game',
                        value: '`brain games <id>`'
                    },
                    {
                        name: 'Create a new game',
                        value: '`brain games make <id> : <name> ; <system> ; <time> ; <gm> ; <players>`'
                    },
                    {
                        name: 'Change one property of a game',
                        value: '`brain games set <id> <property> : <value>`'
                    },
                    {
                        name: 'Get games by their property',
                        value: '`brain games find <property> : <value>`'
                    }
                ]
            }
        });
        return new Resolver();
    }

    public async minesweeper(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `minesweeper` command',
                description: '|| finally some use for spoilers ||',
                fields: [
                    {
                        name: 'Start a default 8x8 game with 10 mines',
                        value: '`brain minesweeper`'
                    },
                    {
                        name: 'Set a custom board',
                        value: '`brain minesweeper : <width> <height> <mine count>`'
                    }
                ]
            }
        });
        return new Resolver();
    }

    public async custom(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for custom commands',
                description: 'OK, this is pretty much what people use me for.\nBoth command names and values can consist of multiple words.',
                fields: [{
                    name: 'List all commands',
                    value: '`brain list`'
                },
                {
                    name: 'Create or modify a command',
                    value: '`brain make <command name> : <value>`'
                },
                {
                    name: 'Create or modify a command with multiple answers',
                    value: '`brain make <command name> : <value1> ; <value2> ; <value3> ; <etc...>`'
                },
                {
                    name: 'Remove a command',
                    value: '`brain remove <command name>`'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async math(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `imalazyfuck` command',
                description: 'Just use calculator, google, python shell or whatever instead...',
                fields: [{
                    name: 'How to use this',
                    value: '`brain imalazyfuck <math expression>`'
                },
                {
                    name: 'Syntax and expression support documentation',
                    value: 'Can be found [here](https://www.npmjs.com/package/expr-eval). Bear in mind you can\'t use variables.'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async convert(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `convert` command',
                description: 'Because Americans \:flag_my: are stupid.',
                fields: [{
                    name: 'How to use this',
                    value: '`brain convert <value> <source unit> <target unit>`. Use unit SYMBOLS not names, so use \'m\' and not \'meter\' ' +
                        '(names are ambiguous, see: meter vs metre)'
                },
                {
                    name: 'List of supported units',
                    value: 'Can be found [here](https://github.com/ben-ng/convert-units).'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async temp(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `temp` command',
                description: 'Yay for duplicate commands.',
                fields: [{
                    name: 'How to use this',
                    value: '`brain temp <value> <source unit>`. Use c/C or f/F, Kelvins are not supported here (just take temp in °C and add 273.15).'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async cash(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `cash` command',
                description: 'Also pretty useful. Supports a wide variety of FIAT currencies and top 100 magical internet beans.',
                fields: [{
                    name: 'How to use this',
                    value: '`brain cash <value> <source currency> <target currency>`. Only use standard currency symbols (like USD, not $ or whatever).'
                }
                ]
            }
        });
        return new Resolver();
    }

    public async time(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `time` command',
                description: 'This one\'s actually pretty useful to keep track of what time is it for everyone else.',
                fields: [{
                    name: 'Current time in UTC',
                    value: '`brain time`'
                },
                {
                    name: 'List of all all people\'s timezones',
                    value: '`brain time list`'
                },
                {
                    name: 'Get one person\'s time and timezone',
                    value: '`brain time <name>`. Those names are not usernames or nicknames, it\'s just what we felt like naming you.'
                },
                {
                    name: 'Add a new person',
                    value: '`brain time set <name> <timezone>`. Timezone is a number between -12 and 14 inclusive.'
                },
                {
                    name: 'Delet person (RIP Law)',
                    value: '`brain time remove <name>`'
                },
                ]
            }
        });
        return new Resolver();
    }

    public async trivia(msg: Discord.Message, services: Services): Promise<Resolver> {
        msg.channel.send({
            embed: {
                color: 0x079691,
                title: 'Brain Help for `trivia` command',
                description: 'To show off how dumb everyone in this discord is \:ok_hand:.',
                fields: [{
                    name: 'Start trivia with default 20 second timer',
                    value: '`brain trivia`'
                },
                {
                    name: 'Start trivia with custom timer (5-60 seconds)',
                    value: '`brain trivia <time>`'
                },
                {
                    name: 'Print current scores',
                    value: '`brain trivia scores`'
                },
                {
                    name: 'How to answer',
                    value: 'Just type a a letter of the answer. Either uppercase or lowercase, doesn\'t matter. ' +
                        'You can only answer once and that message will get deleted, all further answers sent by you will be ignored. '
                }
                ]
            }
        });
        return new Resolver();
    }
}

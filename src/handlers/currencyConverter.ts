import * as request from 'request-promise-native';
import { CommandHandler, Resolver, Services, IBrainConfig } from '../bot/discordbot';
import * as Discord from 'discord.js';
import { readFileSync } from 'fs';

export class CurrencyConverter extends CommandHandler {
    private fiat: any;
    private crypto: any = {};
    private config: IBrainConfig;
    constructor() {
        super();
        this.config = JSON.parse(readFileSync(__dirname + '/../../../brain_data/config.json', 'utf8'));
        // ! DON'T FORGET TO FIX THIS
        this.reload();
        this.interval();
    }

    public async handler(msg: Discord.Message, services: Services): Promise<Resolver> {
        const processed = msg.content.split(' ');
        if(!isNaN(processed[2] as any) && processed[3] !== undefined && processed[3] != null) {
            const amount = parseFloat(processed[2]);
            const from = processed[3].toUpperCase();
            const to = processed[4] === undefined ? 'USD' : processed[4].toUpperCase();
            let value = 0;

            if(to === from) {
                msg.channel.send(`${processed[2]} ${to} == ${processed[2]} ${to}. You dingus`);
                return;
            }
            if((this.fiat.rates[to] !== undefined || to === 'USD') && (this.fiat.rates[from] !== undefined || from === 'USD')) {
                if(to === 'USD') {
                    value = (amount / this.fiat.rates[from]);
                } else if(from === 'USD') {
                    value = amount * this.fiat.rates[to];
                } else {
                    const usd = (amount / this.fiat.rates[from]);
                    value = (usd * this.fiat.rates[to]);
                }
            } else if((this.fiat.rates[to] !== undefined || to === 'USD') && this.crypto[from] !== undefined) {
                const usd = amount * this.crypto[from].price_usd;
                if(to === 'USD') {
                    value = usd;
                } else {
                    value = usd * this.fiat.rates[to];
                }
            } else if((this.fiat.rates[from] !== undefined || from === 'USD') && this.crypto[to] !== undefined) {
                if(from === 'USD') {
                    value = amount / this.crypto[to].price_usd;
                } else {
                    const usd = (amount / this.fiat.rates[from]);
                    value = (usd / this.crypto[to].price_usd);
                }
            } else if(this.crypto[to] !== undefined && this.crypto[from] !== undefined) {
                const usd = this.crypto[from].price_usd * amount;
                value = usd / this.crypto[to].price_usd;
            } else {
                value = -1;
            }

            if(value !== -1) {
                const fixed = this.fiat.rates[to] === undefined ? 4 : 2;
                msg.channel.send(`${processed[2]} ${from} == ${value.toFixed(fixed)} ${to}`);
            } else {
                msg.channel.send('Sorry, this currency pair doesn\'t exist. Alternatively, something else went wrong.');
            }

            return new Resolver();
        }
    }

    private reload() {
        const self = this;
        request.get('http://data.fixer.io/api/latest?access_key=' + this.config.FIXER_API_KEY)
            .then((str) => {
                self.fiat = JSON.parse(str);
                const USDrate = self.fiat.rates.USD;
                for(const x in self.fiat.rates) {
                    if(self.fiat.rates.hasOwnProperty(x) && self.fiat.rates[x] !== 'USD') {
                        self.fiat.rates[x] = self.fiat.rates[x] / USDrate;
                    }
                }
                self.fiat.base = 'USD';
            })
            .catch((err) => {
                console.log(err);
            });

        request.get('https://api.coinmarketcap.com/v1/ticker/')
            .then((str) => {
                const json = JSON.parse(str);
                for(const i in json) {
                    if(json.hasOwnProperty(i)) {
                        self.crypto[json[i].symbol] = json[i];
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    private interval() {
        const self = this;
        const interval = setInterval(() => {
            self.reload();
        }, 24 * 60 * 60 * 1000);
    }
}

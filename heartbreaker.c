#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define STRLEN 25
#define MAX(a, b) (((a)>(b)) ? (a) : (b))

struct race
{
	char name[STRLEN];
	int reroll;
};

int minfour(int, int, int, int);
int khthree(void);
char *classpick(int **);

int main(int argc, char **argv)
{
	//Vital Statistics
	//Attributes assumes STR, DEX, CON, INT, WIS, CHA down the line.
	int *attributes=(int*)malloc(sizeof(int)*6), reroll=1, tmp;
	char race[STRLEN]="";
	char class[STRLEN]="";
	char element[STRLEN]="";
	int startdosh = 0;
	//VERY LONG LISTS !!BE ADVISED!!
	struct race races[20] = {"Hiver", rand()%6+1, "Misbegotten", rand()%6+1, 
		"Child of the Pines", 2, "Orc", 1, "Olm", 2, "Illithid", 4, "Sprite", 2,
	   	"Kenku", 2, "Machine-Man", 1, "Ibben", 6, "Tari", 3, "Child of the Sun",
	   	5, "Serpent-Folk", 4, "Silicoid", 1, "Saccharine", 2, "Daeva", 6, 
		"Myconid", 3, "Slugman", 4, "Cyclops", 0, "Satyr", 6};
	char weapons[15][STRLEN] = {"Handaxes", "Shortswords", "Daggers", "Cudgels", 
		"Swords", "Axes", "Maces", "Spears", "Longswords", "Greataxes",
	   	"Warhammers", "Halberds", "Bows", "Crossbows", "Guns"};
	char schools[12][STRLEN] = {"Battle Wizard", "Biomancer", "Hydrologist", 
		"Mountain Root Mage", "Pyromancer", "Red Hand", "Space Wizard", 
		"Transmuter", "Animist", "Elementalist", "Necromancer", 
		"Orthodox Wizard"};
	//First Four Spells of Each List, Ordered as Above
	char spells[48][STRLEN] = {/*Battle Wizard*/"Sending", "Magic Armour", 
		"Find Path", "Scry", /*Biomancer*/"Extract Venom", "Mend", "Mutate",
	   	"Stoneskin", /*Hydrologist*/"Control Rope", "Telekinetic Shove",
		"Sending", "Find Path", /*Mountain Root Mage*/"Obedient Stone",
		"Olfactory Revelation", "Light", "Telekinetic Shove", /*Pyromancer*/
		"Oil", "Haze", "Light", "Dry", /*Red Hand*/"Light", "Ease", "Clarity"
		"Control Fire", /*Space Wizard*/"Reach", "Distant Skin",
		"Telekinetic Shove", "Magic Missile", /*Transmuter*/"Rope Snake",
		"Disguise", "Forge", "Disintergrate", /*Animist*/"Powerful Presence",
		"Telekinetic Shove", "Fear", "Control Weather", /*Elementalist*/
		"Circle of Frost", "Control Element", "Dissolve", "Element Breath",
		/*Necromancer*/"Raise Spirit", "Explode Corpse", "Death Mask", "Fear",
		/*Orthodox Wizard*/"Lock", "Knock", "Feather", "Floating Disc"};
	//Some spells need elements.
	char elements[4][STRLEN] = {"Water", "Earth", "Air", "Fire"};
	//Specialists steal the D&D5e core skill list for now.
	char skills[18][STRLEN] = {"Acrobatics", "Animal Handling", "Arcana",
		"Athletics", "Deception", "History", "Insight", "Intimidation",
		"Investigation", "Nature", "Medicine", "Perception", "Performance",
		"Persuasion", "Religion", "Sleight of Hand", "Stealth", "Survival"};
	//Clerics get their gods from Golarion because the PFSRD is accessible.
	char gods[20][STRLEN] = {"Abadar", "Asmodeus", "Calistria","Cayden Cailean",
    	"Desna", "Erastil", "Gorum", "Gozreh", "Iomedae", "Irori", "Lamashtu",
		"Nethys", "Norgorber", "Pharasma", "Rovagug", "Sarenrae", "Shelyn",
		"Torag", "Urgathoa", "Zon-Kuthon"};
	//Initialise PRNG
	srand (time(NULL));
	//Pick Race
	tmp = rand()%20;
	snprintf(race, STRLEN, races[tmp].name);
	reroll = races[tmp].reroll;

	//Time to roll some stats after that nightmare of a block.
	//Handle cyclopes first because they're special snowflakes.
	//Drop lowest by zeroing the index of the lowest digit.
	if (!reroll)
	{
		for (int i = 0; i<6; ++i)
			attributes[i] = khthree();
		int redo = rand()%6+1;
		tmp = khthree();
		attributes[redo-1] = MAX(tmp, attributes[redo-1]);
	}
	//Roll for the pleb races.
	else
	{
		for (int i = 0; i<6; ++i)
			attributes[i] = (rand()%6+rand()%6+rand()%6+3);
		tmp = (rand()%6+rand()%6+rand()%6+3);
		attributes[reroll-1] = MAX(tmp, attributes[reroll-1]);
	}
	//Pick a class.
	snprintf(class, STRLEN, classpick(&attributes));
	//Roll starting wealth;
	startdosh+=(rand()%6+rand()%6);
	//TESTING PURPOSES: PRINT WHAT WE HAVE SO FAR:
	//This isn't testing any more. Don't touch my spaghetti.
	puts("New First-Level Character:");
	printf("Race: %s\t Class: %s\n", race, class);
	printf("STR: %d\nDEX: %d\nCON: %d\nINT: %d\nWIS: %d\nCHA: %d\n",
			attributes[0], attributes[1], attributes[2], attributes[3],
		   	attributes[4], attributes[5]);
	//HIGH T LET'S GO CLASS FEATURES AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	switch(class[0])
	{
		case 'F':
			puts("Deed Die: 1d4");
			printf("Weapon Proficiency: %s\n", weapons[rand()%15]);
			break;
		case 'W':
			//Roll an element just in case.
			snprintf(element, STRLEN, elements[rand()%4]);
			puts("Magic Dice: 1");
			char school[STRLEN];
			int schoolnum = rand()%12;
			snprintf(school, STRLEN, schools[schoolnum]);
			printf("School: %s", school);
			if (school[0]=='E')
				printf(" [%s]", element);
			puts("");
			printf("You may memorise a maximum of %d spells.\n", attributes[3]/3);
			char spell1[STRLEN], spell2[STRLEN];
			int s1i, s2i;
			s1i = rand()%4+schoolnum;
		   	s2i = rand()%4+schoolnum;
			while (s1i == s2i)
				s2i = rand()%4+schoolnum;
			snprintf(spell1, STRLEN, spells[s1i]);
			snprintf(spell2, STRLEN, spells[s2i]);
			printf("You begin with %s and %s memorised.\n", spell1, spell2);
			break;
		case 'C':
			printf("Your patron deity is %s.\n", gods[rand()%20]);
			puts("Favour: 1");
			break;
		case 'S':
			startdosh+=(rand()%6+rand()%6); //Add extra starting gold.
			printf("Skill: %s\n", skills[rand()%18]);
			break;
	}
	puts("HP: 5");
	printf("You have %d silver pieces on hand.\n", startdosh);
	free(attributes);
	return 0;
}

int minfour(int a, int b, int c, int d)
{
	int m = a, i = 0;
	if (b < m)
	{
		m = b;
		i = 1;
	}
	if (c < m)
	{
		m = c;
		i = 2;
	}
	if (d < m)
	{
		i = 3;
	}
	return i;
}

int khthree(void)
{
	int rolls[4], sum=0;
	for (int i = 0; i<4; ++i)
		rolls[i]=(rand()%6+1);
	rolls[minfour(rolls[0], rolls[1], rolls[2], rolls[3])] = 0;
	for (int i = 0; i<4; ++i)
		sum+=rolls[i];
	return sum;
}

char *classpick(int **array)
{
	int index = 0, m = (*array)[0];
	for (int i = 1; i<6; ++i)
	{
		if ((*array)[i] > m)
		{
			m = (*array)[i];
			index = i;
		}
	}
	switch(index)
	{
		case 0:
		case 1:
			return "Fighter";
			break;
		case 3:
			return "Wizard";
			break;
		case 4:
		case 5:
			return "Cleric";
			break;
		default:
			return "Specialist";
			break;
	}
}
